from django.conf.urls import include, url
from upload_form import views
#from upload_form.input_file import input_view
from django.contrib import admin

urlpatterns = [
    url(r'^$', views.form, name = 'form'),
    url(r'^smart_input/$', views.input, name = 'input'),
    url(r'^complete/$', views.complete, name = 'complete'),
    url(r'^list/$', views.list, name = 'list'),
    url(r'^oneprediction/$', views.oneprediction, name = 'oneprediction'),
    url(r'^(?P<label_id>[0-9]+)/results/$', views.results, name = 'results'),
    url(r'^(?P<label_id>[0-9]+)/vote/$', views.vote, name = 'vote'),
    url(r'^smart_annotation/$', views.annotation, name = 'annotation'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),
]
