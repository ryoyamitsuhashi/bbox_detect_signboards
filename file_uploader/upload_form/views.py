from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from PIL import Image
from django.template.context_processors import csrf
from django.conf import settings
from django.http import HttpResponseNotModified, HttpResponse, Http404, HttpResponseRedirect
from .models import Message
from upload_form.models import FileNameModel
from upload_form.models import Choice
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
import sys, os
import subprocess
import base64
import json
import xml.etree.ElementTree as ET
import pickle
from os import listdir, getcwd
from os.path import join
import glob
from PIL import Image
from PIL.ExifTags import TAGS
import cv2
import numpy as np
import shutil

Static_DIR = os.path.dirname(os.path.abspath(__file__)) + '/static/'
signboard_voc_DIR = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/'

#これはいらないかも
UPLOADE_DIR_bbox = os.path.dirname(os.path.abspath(__file__)) + '/static/mapillary/sapporo/bboxImages/'

JSONFILE = os.path.dirname(os.path.abspath(__file__)) + '/static/JSON/'
new_UPLOADE_DIR_image = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/JPEGImages_backup/'
new_UPLOADE_DIR_image_source = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/JPEGImages/'
new_UPLOADE_DIR_bbox = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/bboxImages/'
new_UPLOADE_DIR_prediction = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/prediction/'
new_UPLOADE_DIR_prediction_after = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/prediction_after/'
new_UPLOADE_DIR_label = os.path.dirname(os.path.abspath(__file__)) + '/static/signboard_VOC/Annotations/'

#trainfileにmaster以外の人がラベル付けしたデータを.txtで格納
#masterfileはmasterがラベル付けしたデータを.txtで格納
trainfile = os.path.dirname(os.path.abspath(__file__)) + '/static/data/others.txt'
masterfile = os.path.dirname(os.path.abspath(__file__)) + '/static/data/master.txt'

twoflag = True
filename = {}
filename[0] = {}
filename[1] = {}
filename[2] = {}
filename[3] = {}
bbox = {}
label_data = {}
dict_label_data = {}
my_dict = {}
my_dict['user'] = {}
my_dict["label"] = {}
my_dict["bboxfilename"] = []
my_dict["mapillary"] = {}
my_dict["imagedata"] = []
user_dict = {}
empty_list = []
classes = ["signboard"]

my_dict["prediction_name_img_src"] = []
my_dict["prediction_name_img_src_after"] = []


@csrf_exempt
@login_required
def form(request):
    global twoflag
    if twoflag is False:
        return render(request, 'upload_form/complete.html')
    twoflag = False
    file_list = os.listdir(new_UPLOADE_DIR_image)
    for file_name in file_list:
        root, ext = os.path.splitext(file_name)
        flag = False
        for f in file_list:
            root_1, ext_1 = os.path.splitext(f)
            for i in range(8):
                if root == root_1 + '_{}'.format(i):
                    flag = True
                    break
            if flag:
                break
        if flag:
            continue
        im = Image.open(new_UPLOADE_DIR_image + file_name)
        lon_10 = ''
        lat_10 = ''
        # Exif データを取得
        # 存在しなければそのまま終了 空の辞書を返す
        try:
            exif = im._getexif()
            exif_table = {}
            for tag_id, value in exif.items():
                tag = TAGS.get(tag_id, tag_id)
                exif_table[tag] = value
            try:
                # 経度
                lon = exif_table['GPSInfo'][4]
                # 緯度
                lat = exif_table['GPSInfo'][2]
                lon_10 = lon[0][0] / lon[0][1] + (lon[1][0] / lon[1][1] * 60 + lon[2][0] / lon[2][1]) / 3600
                lat_10 = lat[0][0] / lat[0][1] + (lat[1][0] / lat[1][1] * 60 + lat[2][0] / lat[2][1]) / 3600
            except KeyError:
                pass
        except AttributeError:
            pass
        if ext == '.jpeg' or ext == 'png':
            change_file_name = root + '.jpg'
            os.rename(signboard_voc_DIR + 'JPEGImages/' + file_name, signboard_voc_DIR + 'JPEGImages/' + change_file_name)
            os.rename(new_UPLOADE_DIR_image + file_name, new_UPLOADE_DIR_image + change_file_name)
            abs_name = new_UPLOADE_DIR_image + change_file_name
            prediction_name_img_src = '/static/signboard_VOC/prediction/' + change_file_name
            prediction_name_img_src_after = '/static/signboard_VOC/prediction_after/' + change_file_name
            if not my_dict["mapillary"].keys == prediction_name_img_src_after:

                if lon_10:
                    my_dict["mapillary"].update(
                        {prediction_name_img_src_after: {'geometry': {'coordinates': [lon_10, lat_10]}}})
                else:
                    my_dict["mapillary"].update({prediction_name_img_src_after: 0})

            prediction_name = new_UPLOADE_DIR_prediction + change_file_name
            prediction_name_after = new_UPLOADE_DIR_prediction_after + change_file_name
            # 以下各画像に対する処理を記載する
            # 画像の検出処理
            #検出処理については自分のディレクトリの配置によって変更してください。
            if not os.path.isfile(prediction_name_after):
              
                args = ['/volume/darknet/darknet', 'detector', 'test',
                        '/volume/darknet/cfg/ten_city_all_copy.data',
                        '/volume/darknet/cfg/onlysignboard_voc.cfg',
                        '/volume/file_uploader/upload_form/static/backup/ten_city_all_copy_ceres/ten_city_all_copy/onlysignboard_voc_final.weights',
                        '{}'.format(abs_name),
                        '-thresh 0.1']
                try:
                    res = subprocess.check_output(args)

                except subprocess.CalledProcessError as e:
                    res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納
                
                shutil.copyfile('predictions.jpg', prediction_name_after)
        elif ext == '.jpg':
            abs_name = new_UPLOADE_DIR_image + file_name
            prediction_name_img_src = '/static/signboard_VOC/prediction/' + file_name
            prediction_name_img_src_after = '/static/signboard_VOC/prediction_after/' + file_name
            if not my_dict["mapillary"].keys == prediction_name_img_src_after:

                if lon_10:
                    my_dict["mapillary"].update(
                        {prediction_name_img_src_after: {'geometry': {'coordinates': [lon_10, lat_10]}}})
                else:
                    my_dict["mapillary"].update({prediction_name_img_src_after: 0})
            prediction_name = new_UPLOADE_DIR_prediction + file_name
            prediction_name_after = new_UPLOADE_DIR_prediction_after + file_name
            # 以下各画像に対する処理を記載する
            # 画像の検出処理
            if not os.path.isfile(prediction_name_after):
            
                args = ['/volume/darknet/darknet', 'detector', 'test',
                        '/volume/darknet/cfg/ten_city_all_copy.data',
                        '/volume/darknet/cfg/onlysignboard_voc.cfg',
                        '/volume/file_uploader/upload_form/static/backup/ten_city_all_copy_ceres/ten_city_all_copy/onlysignboard_voc_final.weights',
                        '{}'.format(abs_name),
                        '-thresh 0.1']
                try:
                    res = subprocess.check_output(args)


                except subprocess.CalledProcessError as e:
                    res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納

                shutil.copyfile('predictions.jpg', prediction_name_after)
    twoflag = True
    return redirect('upload_form:list')


def input(request):
    global twoflag
    if twoflag is False:
        return render(request, 'upload_form/complete.html')
    twoflag = False
    label_data.clear()
    if request.method != 'POST':
        twoflag = True
        print("Method: GET. Returning base page")

        return render(request, 'upload_form/smart_input.html')
    if len(request.FILES) == 0:
        print("No images uploaded.")
        twoflag = True
        return render(request, 'upload_form/smart_input.html')

    file = request.FILES['myfile']

    path = os.path.join(new_UPLOADE_DIR_image, file.name)
    filename[0][request.user.id] = file.name
    filename[1][request.user.id] = path
    filename[2][request.user.id] = '/static/signboard_VOC/JPEGImages_backup/' + file.name
    filename[3][request.user.id] = new_UPLOADE_DIR_image_source + file.name
    im = Image.open(file)

    # Exif データを取得
    # 存在しなければそのまま終了 空の辞書を返す
    try:
        exif = im._getexif()
    except AttributeError:
        print('Not exif')
        twoflag = True
        return render(request, 'upload_form/smart_input.html')

    # タグIDそのままでは人が読めないのでデコードして
    # テーブルに格納する
    exif_table = {}
    for tag_id, value in exif.items():
        tag = TAGS.get(tag_id, tag_id)
        exif_table[tag] = value
    try:
        # 経度
        lon = exif_table['GPSInfo'][4]
        # 緯度
        lat = exif_table['GPSInfo'][2]
    except KeyError:
        print('Not GPSInfo')
        twoflag = True
        return render(request, 'upload_form/smart_input.html')
    lon_10 = lon[0][0] / lon[0][1] + (lon[1][0] / lon[1][1] * 60 + lon[2][0] / lon[2][1]) / 3600
    lat_10 = lat[0][0] / lat[0][1] + (lat[1][0] / lat[1][1] * 60 + lat[2][0] / lat[2][1]) / 3600


    root, ext = os.path.splitext(filename[0][request.user.id])
    if ext == '.jpeg' or ext == '.png' or ext == '.JPG' or ext == '.JPEG':

        change_file_name = root + '.jpg'


        filename[1][request.user.id] = signboard_voc_DIR + 'JPEGImages_backup/' + change_file_name
        filename[0][request.user.id] = change_file_name
        filename[2][request.user.id] = '/static/signboard_VOC/JPEGImages_backup/' + change_file_name
        filename[3][request.user.id] = new_UPLOADE_DIR_image_source + change_file_name
        destination = open(filename[1][request.user.id], 'wb')
        for chunk in file.chunks():
            destination.write(chunk)

        destination_JPEGImages = open(filename[3][request.user.id], 'wb')
        for chunk in file.chunks():
            destination_JPEGImages.write(chunk)
        insert_data = FileNameModel(file_name=change_file_name)
        insert_data.save()
       
       
        prediction_name_img_src = '/static/signboard_VOC/prediction/' + filename[0][request.user.id]
        prediction_name_img_src_after = '/static/signboard_VOC/prediction_after/' + filename[0][request.user.id]
        if not my_dict["mapillary"].keys == prediction_name_img_src_after:
            my_dict["mapillary"].update({prediction_name_img_src_after: {'geometry': {'coordinates': [lon_10, lat_10]}}})
        prediction_name = new_UPLOADE_DIR_prediction + filename[0][request.user.id]
        prediction_name_after = new_UPLOADE_DIR_prediction_after + filename[0][request.user.id]

        # 以下各画像に対する処理を記載する
        # 画像の検出処理

     
        args = ['/volume/darknet/darknet', 'detector', 'test',
                '/volume/darknet/cfg/ten_city_all_copy.data',
                '/volume/darknet/cfg/onlysignboard_voc.cfg',
                '/volume/file_uploader/upload_form/static/backup/ten_city_all_copy_ceres/ten_city_all_copy/onlysignboard_voc_final.weights',
                filename[1][request.user.id],
                '-thresh 0.1']

        try:
            res = subprocess.check_output(args)


        except subprocess.CalledProcessError as e:
            res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納

        args2 = ['cp', 'predictions.jpg', prediction_name_after]

        try:
            res2 = subprocess.check_output(args2)
        except subprocess.CalledProcessError as e:
            res2 = e.output.decode("utf-8").split("\n")  # リストに文字列を格納

       

        label_data.update({"image": filename[0][request.user.id]})
        k = 0
        for i in range(3, len(res), 2):
            k += 1

            box = res[i].split(",")
            for j in range(len(box)):
                box_number = box[j].split("=")
                if j == 0:
                    label_data.update({"left{}".format(k): box_number[1]})
                if j == 1:
                    label_data.update({"top{}".format(k): box_number[1]})
                if j == 2:
                    label_data.update({"right{}".format(k): box_number[1]})
                if j == 3:
                    label_data.update({"bot{}".format(k): box_number[1]})
            label_data.update({'num': range(k)})
            label_data.update({'k': k})

        dict_label_data["label_data"] = label_data
        return redirect('upload_form:annotation')
    elif ext == '.jpg':
        destination = open(path, 'wb')
        for chunk in file.chunks():
            destination.write(chunk)
        destination_JPEGImages = open(filename[3][request.user.id], 'wb')
        for chunk in file.chunks():
            destination_JPEGImages.write(chunk)
        insert_data = FileNameModel(file_name=file.name)
        insert_data.save()

        prediction_name_img_src = '/static/signboard_VOC/prediction/' + filename[0][request.user.id]
        prediction_name_img_src_after = '/static/signboard_VOC/prediction_after/' + filename[0][request.user.id]
        if not my_dict["mapillary"].keys == prediction_name_img_src_after:
            my_dict["mapillary"].update({prediction_name_img_src_after: {'geometry': {'coordinates': [lon_10, lat_10]}}})
        prediction_name = new_UPLOADE_DIR_prediction + filename[0][request.user.id]
        prediction_name_after = new_UPLOADE_DIR_prediction_after + filename[0][request.user.id]
        # 以下各画像に対する処理を記載する
        # 画像の検出処理

        args = ['/volume/darknet/darknet', 'detector', 'test',
                '/volume/darknet/cfg/ten_city_all_copy.data',
                '/volume/darknet/cfg/onlysignboard_voc.cfg',
                '/volume/file_uploader/upload_form/static/backup/ten_city_all_copy_ceres/ten_city_all_copy/onlysignboard_voc_final.weights',
                filename[1][request.user.id],
                '-thresh 0.1']

        try:
            res = subprocess.check_output(args)

        except subprocess.CalledProcessError as e:
            res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納



        shutil.copyfile('predictions.jpg', prediction_name_after)

        label_data.update({"image": filename[0][request.user.id]})
        k = 0
        for i in range(3, len(res), 2):
            k += 1
            box = res[i].split(",")

            for j in range(len(box)):
                box_number = box[j].split("=")

                if j == 0:
                    label_data.update({"left{}".format(k): box_number[1]})
                if j == 1:
                    label_data.update({"top{}".format(k): box_number[1]})
                if j == 2:
                    label_data.update({"right{}".format(k): box_number[1]})
                if j == 3:
                    label_data.update({"bot{}".format(k): box_number[1]})
            label_data.update({'num': range(k)})
            label_data.update({'k': k})
        dict_label_data["label_data"] = label_data
        twoflag = True
        return redirect('upload_form:annotation')


def oneprediction(request):
    global twoflag
    label_data.clear()
    insert_data = FileNameModel(file_name=filename[0][request.user.id])
    insert_data.save()
    # 画像の検出処理
   
    args = ['/volume/darknet/darknet', 'detector', 'test',
            '/volume/darknet/cfg/ten_city_all_copy.data',
            '/volume/darknet/cfg/onlysignboard_voc.cfg',
            '/volume/file_uploader/upload_form/static/backup/ten_city_all_copy_ceres/ten_city_all_copy/onlysignboard_voc_final.weights',
            filename[1][request.user.id],
            '-thresh 0.1']
    try:
        res = subprocess.check_output(args)
    except subprocess.CalledProcessError as e:
        res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納

    label_data.update({"image": filename[0][request.user.id]})
    k = 0
    for i in range(3, len(res), 2):
        k += 1
        box = res[i].split(",")
        for j in range(len(box)):
            box_number = box[j].split("=")
            if j == 0:
                label_data.update({"left{}".format(k): box_number[1]})
            if j == 1:
                label_data.update({"top{}".format(k): box_number[1]})
            if j == 2:
                label_data.update({"right{}".format(k): box_number[1]})
            if j == 3:
                label_data.update({"bot{}".format(k): box_number[1]})
        label_data.update({'num': range(k)})
        label_data.update({'k': k})

    dict_label_data["label_data"] = label_data
    return redirect('upload_form:annotation')


def convert(size, box):
    dw = 1. / (size[0])
    dh = 1. / (size[1])
    x = (box[0] + box[1]) / 2.0 - 1
    y = (box[2] + box[3]) / 2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return (x, y, w, h)


# ヒストグラム均一化
def equalizeHistRGB(src):
    RGB = cv2.split(src)
    Blue = RGB[0]
    Green = RGB[1]
    Red = RGB[2]
    for i in range(3):
        cv2.equalizeHist(RGB[i])

    img_hist = cv2.merge([RGB[0], RGB[1], RGB[2]])
    return img_hist


# ガウシアンノイズ
def addGaussianNoise(src):
    row, col, ch = src.shape
    mean = 0
    var = 0.1
    sigma = 15
    gauss = np.random.normal(mean, sigma, (row, col, ch))
    gauss = gauss.reshape(row, col, ch)
    noisy = src + gauss

    return noisy


# salt&pepperノイズ
def addSaltPepperNoise(src):
    row, col, ch = src.shape
    s_vs_p = 0.5
    amount = 0.004
    out = src.copy()
    # Salt mode
    num_salt = np.ceil(amount * src.size * s_vs_p)
    coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in src.shape]
    out[coords[:-1]] = (255, 255, 255)

    # Pepper mode
    num_pepper = np.ceil(amount * src.size * (1. - s_vs_p))
    coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in src.shape]
    out[coords[:-1]] = (0, 0, 0)
    return out


def annotation(request):
    global twoflag
    ftitle, fext = os.path.splitext(filename[0][request.user.id])
    labelfilename = new_UPLOADE_DIR_label + ftitle + '.xml'
    bboxfilename = ftitle + '.jpg'
    if "imagedata" in request.POST:
        if twoflag is False:
            return render(request, 'upload_form/complete.html')
        twoflag = False
        # ルックアップテーブルの生成
        min_table = 50
        max_table = 205
        diff_table = max_table - min_table
        gamma1 = 0.75
        gamma2 = 1.5

        LUT_HC = np.arange(256, dtype='uint8')
        LUT_LC = np.arange(256, dtype='uint8')
        LUT_G1 = np.arange(256, dtype='uint8')
        LUT_G2 = np.arange(256, dtype='uint8')

        LUTs = []

        # 平滑化用
        average_square = (10, 10)

        # ハイコントラストLUT作成
        for i in range(0, min_table):
            LUT_HC[i] = 0

        for i in range(min_table, max_table):
            LUT_HC[i] = 255 * (i - min_table) / diff_table

        for i in range(max_table, 255):
            LUT_HC[i] = 255

        # その他LUT作成
        for i in range(256):
            LUT_LC[i] = min_table + i * (diff_table) / 255
            LUT_G1[i] = 255 * pow(float(i) / 255, 1.0 / gamma1)
            LUT_G2[i] = 255 * pow(float(i) / 255, 1.0 / gamma2)

        LUTs.append(LUT_HC)
        LUTs.append(LUT_LC)
        LUTs.append(LUT_G1)
        LUTs.append(LUT_G2)
        # 画像の読み込み
        img_src = cv2.imread(filename[1][request.user.id], 1)
        trans_img = []
        # trans_img.append(img_src)

        # LUT変換
        for i, LUT in enumerate(LUTs):
            trans_img.append(cv2.LUT(img_src, LUT))

        # 平滑化
        trans_img.append(cv2.blur(img_src, average_square))

        # ヒストグラム均一化
        trans_img.append(equalizeHistRGB(img_src))

        # ノイズ付加
        trans_img.append(addGaussianNoise(img_src))
        trans_img.append(addSaltPepperNoise(img_src))

        # 保存
        base = os.path.splitext(os.path.basename(filename[1][request.user.id]))[0] + "_"
        img_src.astype(np.float64)
        for i, img in enumerate(trans_img):
            # 比較用
            # cv2.imwrite("trans_images/" + base + str(i) + ".jpg" ,cv2.hconcat([img_src.astype(np.float64), img.astype(np.float64)]))
            cv2.imwrite(new_UPLOADE_DIR_image + base + str(i) + ".jpg", img)
            cv2.imwrite(signboard_voc_DIR + 'JPEGImages/' + base + str(i) + ".jpg", img)
        img = Image.open(filename[1][request.user.id])
        width, height = img.size
        if 'left1' in request.POST:
            for i in range(1, 50):
                try:
                    if float(request.POST.__getitem__('right{}'.format(i))) < float(
                            request.POST.__getitem__('left{}'.format(i))):
                        left = request.POST.__getitem__('right{}'.format(i))
                        right = request.POST.__getitem__('left{}'.format(i))

                    else:
                        left = request.POST.__getitem__('left{}'.format(i))
                        right = request.POST.__getitem__('right{}'.format(i))
                    if float(request.POST.__getitem__('top{}'.format(i))) > float(
                            request.POST.__getitem__('bot{}'.format(i))):
                        top = request.POST.__getitem__('bot{}'.format(i))
                        bot = request.POST.__getitem__('top{}'.format(i))

                    else:
                        top = request.POST.__getitem__('top{}'.format(i))
                        bot = request.POST.__getitem__('bot{}'.format(i))
                except KeyError:
                    pass
                if "left{}".format(i) in request.POST:
                    if i == 1:
                        f = open(labelfilename, 'w')
                        f.write(
                            '<annotation>\n\t<size>\n\t\t<width>{0}</width>\n\t\t<height>{1}</height>\n\t</size>'.format(
                                width, height))
                    else:
                        f = open(labelfilename, 'a')
                    f.write(
                        '\n\t<object>\n\t\t<name>signboard</name>\n\t\t<difficult>0</difficult>\n\t\t<bndbox>\n\t\t\t<xmin>{0}</xmin>\n\t\t\t<ymin>{1}</ymin>\n\t\t\t<xmax>{2}</xmax>\n\t\t\t<ymax>{3}</ymax>\n\t\t</bndbox>\n\t</object>\n'.format(
                            left,
                            top, right,
                            bot))  # ファイルに文字列を書き込む
            f = open(labelfilename, 'a')
            f.write('</annotation>')
            f.close()

            
            for p in range(8):
                shutil.copyfile(labelfilename, new_UPLOADE_DIR_label + ftitle + '_{}.xml'.format(p))

        dec_file = base64.b64decode(request.POST.__getitem__('imagedata'))
        outfile_path = os.path.join(new_UPLOADE_DIR_bbox, bboxfilename)
        f = open(outfile_path, "wb")
        f.write(dec_file)
        f.close()

        if os.path.isfile(signboard_voc_DIR + 'Annotations/%s.xml' % (ftitle)):
            in_file = open(signboard_voc_DIR + 'Annotations/%s.xml' % (ftitle))
            out_file = open(signboard_voc_DIR + 'labels/%s.txt' % (ftitle), 'w')
            tree = ET.parse(in_file)
            root = tree.getroot()
            size = root.find('size')
            w = int(size.find('width').text)
            h = int(size.find('height').text)
            for obj in root.iter('object'):
                difficult = obj.find('difficult').text
                cls = obj.find('name').text
                if cls not in classes or int(difficult) == 1:
                    continue
                cls_id = classes.index(cls)
                xmlbox = obj.find('bndbox')
                b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text),
                     float(xmlbox.find('ymax').text))
                bb = convert((w, h), b)
                out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')
            out_file.close()
            for k in range(8):
                shutil.copyfile(signboard_voc_DIR + 'labels/{}.txt'.format(ftitle),signboard_voc_DIR + 'labels/{0}_{1}.txt'.format(ftitle,k))
        if request.user.id == 1:
            print('master')
            if not os.path.exists(masterfile):
                f = open(masterfile, 'w')
                f.close()

            for line in open(masterfile, 'r'):
                item = line

                if item.rstrip('\n') == filename[3][request.user.id]:
                    break
            else:
                if os.path.isfile(signboard_voc_DIR + 'Annotations/%s.xml' % (ftitle)):
                    f = open(masterfile, 'a')
                    f.write(filename[3][request.user.id] + '\n')
                    for i, img in enumerate(trans_img):
                        f.write(signboard_voc_DIR + 'JPEGImages/{0}_{1}'.format(ftitle, i) + ".jpg" + '\n')
                    f.close()
        else:
            print('others')
            if not os.path.exists(trainfile):
                f = open(trainfile, 'w')
                f.close()

            for line in open(trainfile, 'r'):
                item = line

                if item.rstrip('\n') == filename[3][request.user.id]:
                    break
            else:
                if os.path.isfile(signboard_voc_DIR + 'Annotations/%s.xml' % (ftitle)):
                    f = open(trainfile, 'a')
                    f.write(filename[3][request.user.id] + '\n')
                    for i, img in enumerate(trans_img):
                        f.write(signboard_voc_DIR + 'JPEGImages/{0}_{1}'.format(ftitle,i) + ".jpg" + '\n')
                    f.close()
        twoflag = True
        return redirect('upload_form:list')
    elif request.POST:
        twoflag = True
        # return redirect('upload_form:complete')
        return redirect('upload_form:list')
    else:

        render_dict = {"label_data": {
            "image": None,
            "top": [],
            "left": [],
            "right": [],
            "bot": []
        }
        }
        for k in sorted(dict_label_data["label_data"].keys(), key=lambda x: (len(x), x)):
            if k == "image":
                render_dict["label_data"][k] = filename[2][request.user.id]  # dict_label_data["label_data"][k]
            else:
                if "top" in k:
                    render_dict["label_data"]["top"].append(int(dict_label_data["label_data"][k]))
                elif "left" in k:
                    render_dict["label_data"]["left"].append(int(dict_label_data["label_data"][k]))
                elif "right" in k:
                    render_dict["label_data"]["right"].append(int(dict_label_data["label_data"][k]))
                elif "bot" in k:
                    render_dict["label_data"]["bot"].append(int(dict_label_data["label_data"][k]))
        twoflag = True
        return render(request, 'upload_form/smart_annotation.html', render_dict)


def complete(request):
    return render(request, 'upload_form/complete.html')


@login_required
def list(request):
    print('User:{}'.format(request.user))
    global twoflag
    if twoflag is False:
        return render(request, 'upload_form/complete.html')
    twoflag = False
    if request.POST:



        try:
            Message.objects.get(pk=request.user.id)
        except:
            msg = Message(user=request.user)
            msg.save()
            print(msg.id)
        user = get_object_or_404(Message, pk=request.user.id)
        user.label_number += 1
        user.save()
        user_dict.update({request.user: user.label_number})

        for k,v in sorted(user_dict.items(), key=lambda x: -x[1]):

            my_dict['user'].update({k:v})
        #for v in sorted(user_dict.values(), reverse=True):
        file = os.path.basename(request.POST.__getitem__('imagedata'))
        file1 = new_UPLOADE_DIR_image + file
        # 要修正！！！
        directory = request.POST.__getitem__('directorydata').rstrip('prediction/' + file)
        # filename[0]がファイル名、[1]が絶対パス、[2]がimg.src, [3]がvocのディレクトリ
        filename[0][request.user.id] = file

        filename[1][request.user.id] = file1

        filename[2][request.user.id] = '/static/signboard_VOC/JPEGImages_backup/' + file

        filename[3][request.user.id] = new_UPLOADE_DIR_image_source + file

        return redirect('upload_form:oneprediction')

    imagekey = []
    file_list = sorted(glob.glob('{}*.json'.format(JSONFILE)))

    for m in file_list:
        j = 0
        k = 0
        with open(m, 'r') as f:
            try:
                json_dict = json.load(f)
            except:
                pass
        # imagekeyを取得
        for i in json_dict['features']:
            if not json_dict['features'][j]['properties']['key'] in imagekey:
                imagekey.append(json_dict['features'][j]['properties']['key'])
            j += 1

    for ik in imagekey:

        # 画像情報を辞書に格納
        ik_DIR_NAME = '/static/signboard_VOC/prediction/' + ik + '.jpg'
        ik_DIR_NAME_after = '/static/signboard_VOC/prediction_after/' + ik + '.jpg'
        #clientIDの記入をしてください。
        try:
            if not my_dict["mapillary"][ik_DIR_NAME_after]:
                if os.path.getsize(Static_DIR + 'GIS.json'):
                    with open(Static_DIR + 'GIS.json', 'r') as gis:
                        json_dict_1 = json.load(gis)

                        # 要チェック
                    for o in range(len(json_dict_1)):

                        b = int(o)

                        if json_dict_1[b]['properties']['key'] == ik:
                            dict_res4 = json_dict_1[b]

                            my_dict["mapillary"][ik_DIR_NAME_after] = dict_res4

                            break
                    else:
                        args3 = ['curl',
                                 'https://a.mapillary.com/v3/images/{}?client_id='.format(
                                     ik)]
                        try:
                            res3 = subprocess.check_output(args3).decode("utf-8")

                            dict_res3 = json.loads(res3)
                            json_dict_1.append(dict_res3)
                            with open(Static_DIR + 'GIS.json', 'w') as gis:
                                json.dump(json_dict_1, gis)

                            my_dict["mapillary"][ik_DIR_NAME_after] = dict_res3

                        except subprocess.CalledProcessError as e:
                            res3 = e.output.decode("utf-8")
                else:
                    args3 = ['curl',
                             'https://a.mapillary.com/v3/images/{}?client_id='.format(
                                 ik)]
                    try:
                        res3 = subprocess.check_output(args3).decode("utf-8")

                        dict_res3 = json.loads(res3)
                        dict_res3_list = [dict_res3]
                        with open(Static_DIR + 'GIS.json', 'w') as gis:
                            json.dump(dict_res3_list, gis)

                        my_dict["mapillary"][ik_DIR_NAME_after] = dict_res3

                    except subprocess.CalledProcessError as e:
                        res3 = e.output.decode("utf-8")
        except KeyError:

            if os.path.getsize(Static_DIR + 'GIS.json'):
                with open(Static_DIR + 'GIS.json', 'r') as gis:
                    json_dict_1 = json.load(gis)


                for o in range(len(json_dict_1)):

                    b = int(o)

                    if json_dict_1[b]['properties']['key'] == ik:
                        dict_res4 = json_dict_1[b]

                        my_dict["mapillary"][ik_DIR_NAME_after] = dict_res4

                        break
                else:
                    args3 = ['curl',
                             'https://a.mapillary.com/v3/images/{}?client_id='.format(
                                 ik)]
                    try:
                        res3 = subprocess.check_output(args3).decode("utf-8")

                        dict_res3 = json.loads(res3)
                        json_dict_1.append(dict_res3)
                        with open(Static_DIR + 'GIS.json', 'w') as gis:
                            json.dump(json_dict_1, gis)

                        my_dict["mapillary"][ik_DIR_NAME_after] = dict_res3

                    except subprocess.CalledProcessError as e:
                        res3 = e.output.decode("utf-8")
            else:
                args3 = ['curl',
                         'https://a.mapillary.com/v3/images/{}?client_id='.format(
                             ik)]
                try:
                    res3 = subprocess.check_output(args3).decode("utf-8")

                    dict_res3 = json.loads(res3)
                    dict_res3_list = [dict_res3]
                    with open(Static_DIR + 'GIS.json', 'w') as gis:
                        json.dump(dict_res3_list, gis)

                    my_dict["mapillary"][ik_DIR_NAME_after] = dict_res3

                except subprocess.CalledProcessError as e:
                    res3 = e.output.decode("utf-8")


    latest_label_list = FileNameModel.objects.order_by('-upload_time')[:5]
    context = {'latest_label_list': latest_label_list}
    my_dict["bbox"] = bbox

    bboxdirectory = os.listdir(UPLOADE_DIR_bbox)
    bboxdirectory.extend(os.listdir(new_UPLOADE_DIR_bbox))
    for b in bboxdirectory:
        ftitle, fext = os.path.splitext(b)
        if b not in my_dict['bboxfilename']:
            my_dict['bboxfilename'].append(b)
        if os.path.isfile(signboard_voc_DIR + 'JPEGImages_backup/' + b):
            if not os.path.isfile(signboard_voc_DIR + 'JPEGImages_fix/' + b):
                shutil.move(signboard_voc_DIR + 'JPEGImages_backup/' + b, signboard_voc_DIR + 'JPEGImages_fix')
                if os.path.isfile(signboard_voc_DIR + 'JPEGImages_backup/' + ftitle + '_0' + fext):
                    for i in range(8):
                        shutil.move(signboard_voc_DIR + 'JPEGImages_backup/' + ftitle + '_{}'.format(i) + fext, signboard_voc_DIR + 'JPEGImages_fix')


    for latestlabel in latest_label_list:
        label = get_object_or_404(FileNameModel, pk=latestlabel.id)
        if not label.choice_set.all():
            label.choice_set.create(choice_text='good', votes=0)
            label.choice_set.create(choice_text='bad', votes=0)
        ftitle, fext = os.path.splitext(label.file_name)
        bboxfilename = ftitle + '.jpg'
        if bboxfilename not in my_dict['bboxfilename']:
            my_dict['bboxfilename'].append(bboxfilename)

    if my_dict['bboxfilename']:
        key_list = [os.path.basename(k) for k in my_dict['mapillary'].keys()]

        for f in my_dict['bboxfilename']:
            if f in key_list:
                if isinstance(my_dict['mapillary']['/static/signboard_VOC/prediction_after/' + f], int):
                    my_dict['mapillary']['/static/signboard_VOC/prediction_after/' + f] = {}
                my_dict['mapillary']['/static/signboard_VOC/prediction_after/' + f][
                    'bboxfilename'] = '/static/signboard_VOC/bboxImages/' + f
    twoflag = True

    return render(request, 'upload_form/list.html', my_dict)


def results(request, label_id):
    label = get_object_or_404(FileNameModel, pk=label_id)
    return render(request, 'upload_form/results.html', {'label': label})


def vote(request, label_id):
    label = get_object_or_404(FileNameModel, pk=label_id)
    try:
        selected_choice = label.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'upload_form/detail.html', {
            'label': label,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()

        return HttpResponseRedirect(reverse('upload_form:results', args=(label.id,)))

# Create your views here.
