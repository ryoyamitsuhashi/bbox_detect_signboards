# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
plt.rcParams['font.family'] = 'IPAPGothic' #全体のフォントを設定
color_list = ['red','blue','yellow','pink','black','green','orange','brown','aqua','lightgrey','gold','olive','lightsalmon','violet','grey','indianred','cornsilk','lightgreen']
argvs = sys.argv
argc = len(sys.argv)
for i,argv in enumerate(argvs[1:]):
	k = 0
	x = []
	y = []
	print_dict = {}
	recall_precision = {}
	x_dict = {}
	y_dict = {}
	total = 0
	print('argv:{}'.format(argv))
	with open(argv,"r") as fout:
		for line in fout:
			elements = line.rstrip().split(':')
			x.append(elements[8].rstrip('%'))
			y.append(elements[6].rstrip('%\tIOU'))
			if not elements[6].rstrip('%\tIOU') == '-nan':
				print_dict.update({"thresh_{}".format(k):[elements[8].rstrip('%'),elements[6].rstrip('%\tIOU')]})
			k += 0.01
	plt.scatter(x, y,c = '{}'.format(color_list[i]),label="{}".format(argv.rstrip('.txt')))

	for re,pre in print_dict.values():
		if recall_precision.items():
			for re1,pre1 in recall_precision.items():
				if re == re1 and pre < pre1:
					break
			else:
				recall_precision.update({re:pre})
		else:
			recall_precision.update({re:pre})
	for k,v in recall_precision.items():
		total += float(v)
	mean = total/len(recall_precision.values())
	print('{0}:{1}'.format(argv,mean))

plt.xlabel("Recall")
plt.ylabel("Precision")
plt.legend()
plt.show()