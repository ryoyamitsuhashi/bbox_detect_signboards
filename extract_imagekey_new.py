import json
import subprocess
import os
import sys
#ここに名前を
station = ''
JSONFILE = '{}_1000.json'.format(station)
if not os.path.exists("{}".format(station)):
	os.mkdir(station)
	os.mkdir("{}/radius_1000".format(station))
	os.mkdir("{}/radius_1000/JPEGImages".format(station))
#closeto=経度,緯度
#client_id=クライアントID
args = ['curl', "https://a.mapillary.com/v3/images?closeto=,&radius=1000&client_id=", '-o', "{0}/radius_1000/{1}".format(station,JSONFILE)]
try:
	res = subprocess.check_output(args)
except subprocess.CalledProcessError as e:
        res = e.output.decode("utf-8").split("\n")  # リストに文字列を格納
imagekey = []
j=0
k=0
f = open('{0}/radius_1000/{1}'.format(station,JSONFILE),'r')
json_dict = json.load(f)
for i in json_dict['features']:
	imagekey.append(json_dict['features'][j]['properties']['key'])
	j+=1
print("key:{}".format(imagekey))
 #mapillaryから画像情報を取得
for i in imagekey:
	args = ['curl', 'https://d1cuyjsrcm0gby.cloudfront.net/{}/thumb-2048.jpg'.format(imagekey[k]), '-o', '{0}/radius_1000/JPEGImages/{1}.jpeg'.format(station,imagekey[k])]
	k+=1
	try:
		res2 = subprocess.check_output(args)
	except subprocess.CalledProcessError as e:
		res2 = e.output.decode("utf-8").split("\n")  # リストに文字列を格納

